//
// Berlin Brown
// bigbinc@hotmail.com
//
// connect.c
// 
// - the entry point for the networking code
//
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include <float.h>              // used for _control


#include "../keys.h"
#include "include/connect.h"
#include "include/clients.h"
#include "network.h"

//
// The network message will keep the user informed
// of what is going on at the bottom of the screen
//
static char network_msg[80] = "Use TAB to switch network modes";
static int _cur_mode = -1;


//
// Super - FUNCTION
// - must be called in main.c(glant.c)
//
void Super_InitNetwork(void)
{
  
  Snapshot_StartTime();
  
  Create_Client_List();
  Create_Client();        // simple inits

} // end of the function 

//
// Super_DeleteNetwork
//
void Super_DeleteNetwork(void)
{
  Delete_Client_List();
  Kill_Client();
  Kill_Server();

} // end of the function 

//
// Get_NetworkMsg
//
void Get_NetworkMsg(char *buffer)
{

  strcpy(buffer, network_msg);

} // end of the function

//
// Set_NetworkMsg(char *buffer)
//
void Set_NetworkMsg(char *buffer)
{
  strcpy(network_msg, buffer);
} // end of func

// we really should use final_str more
//


//
// Start_Service()
// - the only input we need is the string
// with the parameters for starting the service
//
// mode: client or server
void Start_Service(char final_str[4][80], int mode)
{
  char buffer[80];

  strcpy(buffer, final_str[1]);  // get the connect str

  switch(mode)
    {
    case SEL_CLIENT_MODE:

      _cur_mode = SEL_CLIENT_MODE;

      // connect to server, this is so fun!x
      Connect_Server(buffer);
      Set_NetworkMsg("Connecting to Server");
      break;

    case SEL_SERVER_MODE:

      // At this point, the user wants
      // to start a server
      // first get the current ip
      // the user should know already, but it doesnt hurt
      Get_LocalAddress();
      Set_NetworkMsg("Getting Local IP Address");

      _cur_mode = SEL_SERVER_MODE;

      // starting server
      Launch_Server();
      //Kill_Server();

      Set_NetworkMsg("Launching Server");
      break;
      
    default: break;
    };

} // end of the function 

//
// Print_NetRun
// - print if the network is running or not
//
void Print_NetRun(void)
{
  if (_cur_mode == SEL_CLIENT_MODE)
    {
      Print_ClientRun();
    } else if (_cur_mode == SEL_SERVER_MODE) {
      
      Print_ServerRun();
      
    } // end of the if

} // end of the function 
