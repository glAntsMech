//
// walls.h
//
#ifndef _WALLS_H_
#define _WALLS_H_

// all of these are in pyramid.cpp for some strange reason

#define WALL_HEIGHT				12.0f


void Draw_Wall_List(void);
void Create_Wall_List(void);
void Delete_Wall_List(void);
void Print_Wall_List(void);

void CreateWalls(void);

#endif

