//
// Berlin Brown
// bigbinc@hotmail.com
//
#ifndef _CAMERA_H_
#define _CAMERA_H_

#include "bot.h"

#define MAX_ZOOM			45.2f

#define OFFSET_ROTATION		8.0f

const float PI		= 3.14159265358f;
const float PI_180	= PI / 180.0f;

#define CAMERA		driver_camera[current_camera]

typedef float Vector[3];

#define MAX_CAMERAS		10	


#define BEGIN_CAMERA 	glPushMatrix(); SetCamera();		
#define END_CAMERA		glPopMatrix();

#define SELECT_CAMERA(cam_id)	current_camera = cam_id

#define CAMERA_STATIC			0
#define CAMERA_WALKING			1

//----------------------------------
// define a camera struct
//..................................
typedef struct tagCamera 
{

	float position[3];			// current location
	float angle[3];				// angle camera is pointing
	float rotation[3];			// rotation around the world
	float centerx;				// center axis
	float centery;
	float centerz;				// center axis x, y, z

	float Yaw;
	float Pitch;
	float Roll;

	float	zoom_factor;
	float	old_zoom;			// save zoom

	int		id;					// id number for camera
	int		type;				// camera TYPE
	
} DriverCamera;


void Vector_Normalize(Vector a, Vector res);

void HandleCameraKeys(bool *keys);


float GetBotX(void);
float GetBotY(void);

void LoadCameras(void);
void ToggleCamera(void);
void SetCamera(void);

void TranslateCamera(float x, float y, float z);
void AngleCamera(float x, float y, float z);
void TurnCamera(float x, float y, float z);

void PosCamera(float x, float y, float z);

void SyncCamera(void);

void GetCameraBot(DriverBotPtr bot);

void SpringDamp(	
		Vector currPos,
		Vector trgPos,     // Target Position
		Vector prevTrgPos, // Previous Target Position
		Vector result,

		float springConst,  // Hooke's Constant
		float dampConst,    // Damp Constant
		float springLen);

extern DriverCamera *driver_camera[MAX_CAMERAS];
extern int	current_camera;

void ToggleViewMode(void);


#endif